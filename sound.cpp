#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>

#include <qobject.h>
#include <qlabel.h>
#include <qsocknot.h>
#include <qgrpbox.h>

#include <kapp.h>
#include <klocale.h>

#include "sound.moc"

#define _(TEXT) globalKlocale->translate(TEXT)

#define SPACE 12

extern KApplication *globalKapp;
extern KLocale      *globalKlocale;

/* ------------------------------------------------------------------------ */

SoundOptions::SoundOptions(Soundcard *c, const char *name)
    : KTopLevelWidget(name)
{
    static char *rates[] = {
	"8000","11025","16000","22050","32000","44100","48000",NULL};
    static char *triggers[] = {
	"0","2","5","8","12","20",NULL};
    unsigned int x,y,w,h,j;
    int i;
    QSize size;
    
    card = c;
    
    /* create widgets */
    QGroupBox *f1 = new QGroupBox(this);
    f1->setTitle(card->driver());

    tab[0][0] = new QLabel(_("audio format"), this, "lformat");
    tab[0][1] = new QLabel(_("# of channels"), this, "lchannels");
    tab[0][2] = new QLabel(_("sample rate"), this, "lrate");
    tab[0][3] = new QLabel(_("record trigger"), this, "ltrigger");

    tab[1][0] = format = new QComboBox(FALSE, this, "format");
    for (i = 1; i <= FMT_MAX; i <<= 1)
	if (1 == card->has_format(i))
	    format->insertItem(sndfmt2str(i),-1);

    tab[1][1] = channels = new QComboBox(FALSE, this, "channels");
    channels->insertItem(_("mono"),-1);
    if (2 == card->has_channels())
	channels->insertItem(_("stereo"),-1);

    tab[1][2] = rate = new QComboBox(TRUE, this, "rate");
    for (i = 0; rates[i] != NULL; i++)
	rate->insertItem(rates[i],-1);

    tab[1][3] = trigger = new QComboBox(TRUE, this, "trigger");
    for (i = 0; triggers[i] != NULL; i++)
	trigger->insertItem(triggers[i],-1);

    /* init */
    for (j = 0; j < sizeof(tabw)/sizeof(int); j++) tabw[j] = 0;
    for (j = 0; j < sizeof(tabh)/sizeof(int); j++) tabh[j] = 0;

    /* get max sizes for cols/rows */
    for (y = 0; y < sizeof(tabh)/sizeof(int); y++) {
	for (x = 0; x < sizeof(tabw)/sizeof(int); x++) {
	    size = tab[x][y]->sizeHint();
	    if (size.isValid()) {
		if (size.width() > tabw[x])
		    tabw[x] = size.width();
		if (size.height() > tabh[y])
		    tabh[y] = size.height();
	    }
	}
    }

    /* arrange widgets */
    for (j = 0; j < sizeof(tabw)/sizeof(int); j++) tabw[j] += SPACE;
    for (y = 0, h = 3*SPACE; y < sizeof(tabh)/sizeof(int); y++, h+=SPACE) {
	for (x = 0, w = 2*SPACE; x < sizeof(tabw)/sizeof(int); x++, w+=SPACE) {
	    tab[x][y]->setGeometry(w,h,tabw[x],tabh[y]);
	    w += tabw[x];
	}
	h += tabh[y];
    }
    h += SPACE;
    w += SPACE;

    /* set frame size */
    f1->setLineWidth(1);
    f1->setFrameStyle(QFrame::Box | QFrame::Sunken);
    f1->setGeometry(SPACE,SPACE,w-2*SPACE,h-2*SPACE);
    h += SPACE;

    /* add buttons */
    cancel = new QPushButton(_("Cancel"), this, "cancel");
    cancel->resize(cancel->sizeHint());
    x = w-SPACE-cancel->width();
    cancel->move(x,h);

    apply = new QPushButton(_("Apply"), this, "apply");
    apply->resize(apply->sizeHint());
    x -= SPACE+apply->width();
    apply->move(x,h);

    ok = new QPushButton(_("OK"), this, "ok");
    ok->resize(ok->sizeHint());
    x -= SPACE+ok->width();
    ok->move(x,h);
    ok->setDefault(TRUE);

    h += ok->height()+SPACE;

    connect(ok, SIGNAL(clicked()), this, SLOT(ok_cb()));
    connect(apply, SIGNAL(clicked()), this, SLOT(apply_cb()));
    connect(cancel, SIGNAL(clicked()), this, SLOT(cancel_cb()));

    connect(card,SIGNAL(newparams(struct SOUNDPARAMS*)),
	    this, SLOT(new_params(struct SOUNDPARAMS*)));

    setCaption(_("sound options"));

#if 1
    /* session management */
    i = -1;
    if (globalKapp->isRestored()) {
	for (i = 1; canBeRestored(i); i++)
	    if (0 == strcmp(classNameOfToplevel(i),"SoundOptions"))
		break;
	if (!canBeRestored(i))
	    i = -1;
    }
    if (i > 0) {
	restore(i);
    } else {
	resize(w,h);
    }
#else
    resize(w,h);
#endif
}

void
SoundOptions::ok_cb()
{
    set_params();
    hide();
}

void
SoundOptions::apply_cb()
{
    set_params();
}

void
SoundOptions::cancel_cb()
{
    hide();
}

/* ---------------------------------------------------------------------- */

static struct {
    int  fmt;
    char *name;
} fmt2str_map [] = {
    { FMT_UNDEFINED, "UNDEFINED" },
    { FMT_8BIT,      "8bit pcm"  },
    { FMT_16BIT,     "16bit pcm" },
    { FMT_MULAW,     "u-law" },
    { FMT_ALAW,      "a-law" },
    { 0, NULL }
};

void
SoundOptions::saveProperties(KConfig *config)
{
    config->writeEntry("rate",current.rate);
    config->writeEntry("channels",current.channels);
    config->writeEntry("format",current.format);
    config->writeEntry("trigger",atoi(trigger->currentText()));
}

void
SoundOptions::readProperties(KConfig *config)
{
    int  i;
    char text[32];

    current.rate     = atoi(config->readEntry("rate"));
    current.channels = atoi(config->readEntry("channels"));
    current.format   = atoi(config->readEntry("format"));
    card->setparams(&current);

    sprintf(text,"%d",atoi(config->readEntry("trigger")));
    for (i = 0; i < trigger->count(); i++)
	if (0 == strcmp(text,rate->text(i))) {
	    trigger->setCurrentItem(i);
	    return;
	}
    trigger->insertItem(text,i);
}

void
SoundOptions::set_params()
{
    int         i;
    const char  *text;

    if (0 < (i = atoi(rate->currentText())))
	current.rate = i;
    current.channels = channels->currentItem()+1;
    text = format->text(format->currentItem());
    for (i = 0; fmt2str_map[i].name != NULL; i++)
	if (0 == strcmp(fmt2str_map[i].name,text))
	    current.format = i;
    card->setparams(&current);
    emit set_level(atoi(trigger->currentText()));
}

void
SoundOptions::new_params(struct SOUNDPARAMS *p)
{
    int i;
    char text[32],*h;
    
    h = sndfmt2str(current.format);
    for (i = 0; i < format->count(); i++)
	if (0 == strcmp(h,format->text(i)))
	    format->setCurrentItem(i);

    memcpy(&current,p,sizeof(struct SOUNDPARAMS));
    channels->setCurrentItem(current.channels-1);

    sprintf(text,"%d",current.rate);
    for (i = 0; i < rate->count(); i++)
	if (0 == strcmp(text,rate->text(i))) {
	    rate->setCurrentItem(i);
	    return;
	}
    rate->insertItem(text,i);
    rate->setCurrentItem(i);
}

char *sndfmt2str(int format)
{
    int i;

    for (i = 0; fmt2str_map[i].name != NULL; i++)
	if (fmt2str_map[i].fmt == format)
	    break;
    return fmt2str_map[i].name;
}
