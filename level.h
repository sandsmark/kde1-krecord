/*
 * level.h. Part of krecord by Gerd Knorr.
 *
 * Displays the input level.
 *
 * Copyright (C) 1998 Florian Kolbe
 *
 * History see level.cpp
 *
 */

#ifndef LEVEL_H
#define LEVEL_H

#include <qwidget.h>

class LevelWindow : public QWidget
{
    Q_OBJECT;

public:
    LevelWindow(QWidget *parent, char *name);
    ~LevelWindow(void);

public slots:
    void new_params(struct SOUNDPARAMS *params);
    void new_data(void *data);
    void resetPeakLeft();
    void resetPeakRight();

protected:
    void paintEvent (QPaintEvent *);
    void resizeEvent(QResizeEvent *);
    
private:
    int           init;
    int           afmt;
    int           samples;
    int           channels;
    signed short* sdata;
    QColor	  orange;
    QPixmap*      buffer;
    QTimer*       timer[2];
    int           peak[2];

    void drawBar(QPainter& painter, int where, int level, int size);
    
};

#endif /* LEVEL_H */
