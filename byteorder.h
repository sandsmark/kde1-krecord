#define ENDIAN_LITTLE    1
#define ENDIAN_BIG       2
#define ENDIAN_THIS_BOX  ENDIAN_LITTLE

/* ---------------------------------------------------------------------- */

#if ENDIAN_THIS_BOX == ENDIAN_LITTLE
# define cpu_to_le32(x) (x)
# define cpu_to_le16(x) (x)
# define le32_to_cpu(x) (x)
# define le16_to_cpu(x) (x)
#endif

#if ENDIAN_THIS_BOX == ENDIAN_BIG
/* TODO */
#endif
