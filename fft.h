#ifndef FFT_H
#define FFT_H

#include <qwidget.h>
#include <X11/Xlib.h>

/* ------------------------------------------------------------------------ */

class FFTWindow : public QWidget
{
    Q_OBJECT;

public:
    FFTWindow(QWidget *parent, char *name);

public slots:
    void new_params(struct SOUNDPARAMS *params);
    void new_data(void *data);

protected:
    void resizeEvent(QResizeEvent *event);
    
private:
    unsigned long  fore,back;
    GC             gc;
    int            *logmap;
    XSegment       *segments;
    short          *buffer;

    int            afmt,channels,rate;
    int            audio_size, fft_size, lmax;
    
    void make_logmap();
    void calculate(unsigned char *data);
    void drawhist();
};

#endif
