#ifndef KRECORD_H
#define KRECORD_H

#include <qdialog.h>
#include <qmsgbox.h>
#include <qpopmenu.h>
#include <qmenubar.h>
#include <qtooltip.h>
#include <qlayout.h>
#include <qpushbt.h>
#include <qchkbox.h>
#include <qbttngrp.h>
#include <qradiobt.h>
#include <qlistbox.h>
#include <qaccel.h>

#include <kapp.h>
#include <kmsgbox.h>
#include <kmenubar.h>
#include <ktopwidget.h>
#include <ktabctl.h>

/* ------------------------------------------------------------------------ */

class KFFT : public KTopLevelWidget
{
    Q_OBJECT;
public:
    KFFT(Soundcard *card);
    ~KFFT();

public slots:
    void showit();

private:
    FFTWindow     *fftwin;
};

class KLevel : public KTopLevelWidget
{
    Q_OBJECT;
public:
    KLevel(Soundcard *card);
    ~KLevel();

public slots:
    void showit();

private:
    LevelWindow *levelwin;
};

/* ------------------------------------------------------------------------ */

class KRecord : public KTopLevelWidget
{
    Q_OBJECT
public:
    KRecord();
    ~KRecord();
    Soundcard     *soundcard;
    SoundOptions  *soundopts;
    QFileDialog   *fdialog;
    QAccel        *accel;
    KFFT          *kfft;
    KLevel        *klevel;
    BufferList    *blist;
    KDNDDropZone  *dropzone;

public slots:
    void nop_cb();
    void new_file();
    void save_as();
    void quit_cb();

    void record_options();
    void exec_mixer();
    void tb_toggle();
    void sl_toggle();

    void help_cb();
    void about_cb();

    void update_statusline(char *text);
    void update_statusline(struct SOUNDPARAMS *p);

    void drop(KDNDDropZone* zone);

private:
    void create_menu();
    void create_toolbar();
    void create_soundbar();
    void create_statusline();

    KMenuBar      *main_menu;
    QPopupMenu    *file_menu;
    QPopupMenu    *opt_menu;
    QPopupMenu    *help_menu;

    KToolBar      *toolbar;
    KToolBar      *soundbar;
    int            tb_id,sb_id,tb_mid,tb_map;

    QListBox      *listwidget;

    KStatusBar    *statusline;
    int            sl_mid,sl_map;
};

#endif
