#ifndef SOUND_H
#define SOUND_H

#include "config.h"
#ifdef HAVE_SYS_SOUNDCARD_H
#include "oss.h"
#endif
#ifdef HAVE_SUN_AUDIOIO_H
#include "sunaudio.h"
#endif

#include <qdialog.h>
#include <qwidget.h>
#include <qcombo.h>
#include <qpushbt.h>

#include <ktopwidget.h>

/* ---------------------------------------------------------------------- */

#define FMT_UNDEFINED  0
#define FMT_8BIT       1          /* unsigned */
#define FMT_16BIT      2          /* signed - native byte order */
#define FMT_MULAW      4          /* NOT SUPPORTED (yet) */
#define FMT_ALAW       8          /* NOT SUPPORTED (yet) */

#define FMT_MAX        2

struct SOUNDPARAMS {
    int format;
    int channels;
    int rate;
    int blocksize;
    int latency;
};

char *sndfmt2str(int format);

/* ---------------------------------------------------------------------- */

class SoundOptions : public KTopLevelWidget /* QDialog */
{
    Q_OBJECT;

public:
    SoundOptions(Soundcard *c, const char *name=0);
    virtual void saveProperties(KConfig *config);
    virtual void readProperties(KConfig *config);

private:
    Soundcard          *card;
    struct SOUNDPARAMS  current;
    void set_params();

    QComboBox          *format;
    QComboBox          *channels;
    QComboBox          *rate;
    QComboBox          *trigger;
    
    QWidget            *tab[2][4];
    int                 tabw[2],tabh[4];

    QPushButton        *ok, *apply, *cancel;
    
signals:
    void set_level(int l);

public slots:
    void new_params(struct SOUNDPARAMS *p);
    void ok_cb();
    void apply_cb();
    void cancel_cb();
};

#endif

