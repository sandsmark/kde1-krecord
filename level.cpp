/*
 * level.cpp. Part of krecord by Gerd Knorr.
 *
 * Displays the input level.
 *
 * Copyright (C) 1998 Florian Kolbe
 *
 * History:
 *
 * Jun 04 1998 Florian Kolbe
 *    Created
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <limits.h>

#include <qwidget.h>
#include <qpixmap.h>
#include <qpainter.h>
#include <qcolor.h>
#include <qtimer.h>

#include "sound.h"
#include "level.moc"

#ifndef min
#define min(a,b) (a < b ? a : b)
#endif

#ifndef max
#define max(a,b) (a > b ? a : b)
#endif

/* ---------------------------------------------------------------------- */

LevelWindow::LevelWindow(QWidget *parent, char *name):QWidget(parent,name,0)
{

   /*
      did not get sound-params yet.
   */
   init  = FALSE;
   sdata = NULL;

   /*
      no peaks initially.
   */
   peak[0] = 0;
   peak[1] = 0;

   /*
      will use output buffer, so no background necessary.
   */
   setBackgroundMode(NoBackground);
   orange = QColor("orangered");

   /*
      Initialize output buffer.
   */   
   buffer = new QPixmap(size());

   /*
      Initialize timers to reset peaks.
   */
   timer[0] = new QTimer();
   timer[1] = new QTimer();
   connect(timer[0], SIGNAL(timeout()), this, SLOT(resetPeakLeft()));
   connect(timer[1], SIGNAL(timeout()), this, SLOT(resetPeakRight()));

} /* LevelWindow */

LevelWindow::~LevelWindow(void) {

   delete buffer;
   delete timer[0];
   delete timer[1];

} /* ~LevelWindow */

void LevelWindow::resizeEvent(QResizeEvent*) {

   /*
      Fix size of output buffer.
   */
   delete buffer;
   buffer = new QPixmap(size());

} /* resizeEvent */

void LevelWindow::resetPeakLeft(void) {

   peak[0] = 0;
   repaint();

} /* resetPeak */

void LevelWindow::resetPeakRight(void) {

   peak[1] = 0;
   repaint();

} /* resetPeak */

void LevelWindow::drawBar(QPainter& painter, int channel, int level, int size) {

   int xLevel = width()*level/100; /* x-pos of current level */
   int x80    = width()*80/100;    /* x-pos of 80% level     */
   int y;

   /*
      Left/mono top, right bottom.
   */
   if (channel == 0) {
      y = 0;
   } else {
      y = height()/2;
   }
  
   /*
      Green: 0%-[level|80%]
   */
   painter.fillRect(0, y+1, max(1, min(xLevel, x80)), size-2, darkGreen);

   /*
      Yellow part.
   */
   if (level > 80) {
      painter.fillRect(x80, y+1, max(1, xLevel-x80), size-2, darkYellow);
   }

   /*
      Current peak is either reached again or pushed.
   */
   if (level >= peak[channel]) {
      peak[channel] = level;
      timer[channel]->start(1000, TRUE);
   }

   /*
      Draw peak if greater than current level.
   */
   if (peak[channel] >= level) {

      /*
          0- 80: green
         80- 98: yellow
         99-100: orange
      */
      painter.setPen(green);
      if (peak[channel] > 80) {
         painter.setPen(yellow);
      }
      if (peak[channel] >= 99) {
         painter.setPen(orange);
      }
      painter.drawLine(min(width()*peak[channel]/100-1,width()-1), y+1,
                       min(width()*peak[channel]/100-1,width()-1), y+size-2);
   }

} /* drawBar */

void LevelWindow::paintEvent(QPaintEvent*)
{
   int      maxLeft  = 0;
   int      maxRight = 0;
   int      i;
   QPainter painter;
   int      maxAmp = 32768;

   if ((init == FALSE) || !sdata) return;

   /*
      Find max amplitude depending on format.
   */
   if (afmt == FMT_16BIT) {

      maxAmp = 32768;
      if (channels == 1) {
         for (i = 0; i < samples; i++)
            if (abs(sdata[i]) > maxLeft) maxLeft = abs(sdata[i]);
      } else
      if (channels == 2) {
         for (i = 0; i < samples; i++) {
            if (abs(sdata[i*2])   > maxLeft)  maxLeft  = abs(sdata[i*2]);
            if (abs(sdata[i*2+1]) > maxRight) maxRight = abs(sdata[i*2+1]);
         }
      }
   } else {
      unsigned char* bdata = (unsigned char*)sdata;

      maxAmp = 128;
      
      if (channels == 1) {
         for (i = 0; i < samples; i++)
            if (abs(bdata[i]-128) > maxLeft) maxLeft = abs(bdata[i]-128);
      } else
      if (channels == 2) {
         for (i = 0; i < samples; i++) {
            if (abs(bdata[i*2]-128)   > maxLeft)  maxLeft  = abs(bdata[i*2]-128);
            if (abs(bdata[i*2+1]-128) > maxRight) maxRight = abs(bdata[i*2+1]-128);
         }
      }
   }

   /*
      Draw bars.
   */
   buffer->fill(black /* backgroundColor() */);
   painter.begin(buffer);

   drawBar(painter, 0, 100*maxLeft/maxAmp, height()/channels);
   if (channels == 2) {
      drawBar(painter, 1, 100*maxRight/maxAmp, height()/channels);
   }

   painter.end();
   bitBlt(this, 0, 0, buffer);
   
} /* paintEvent */

void LevelWindow::new_params(struct SOUNDPARAMS *p)
{
    afmt        = p->format;
    channels    = p->channels;
    samples     = p->blocksize/channels/(afmt == FMT_16BIT ? 2 : 1);

    init = TRUE;

} /* new_params */

void LevelWindow::new_data(void *data)
{

    sdata = (short int*)data;
    repaint();

} /* new_data */
