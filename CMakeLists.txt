cmake_minimum_required (VERSION 3.0)

project (krecord)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_BUILD_WITH_INSTALL_RPATH ON)

find_package(KDE1 REQUIRED)
find_package(X11 REQUIRED)
find_package(PulseAudio REQUIRED)

include_directories (
    ${KDE1_INCLUDE_DIR}
)


include(CMakePackageConfigHelpers)
include(Qt1Macros)
include(KDE1Macros)
include(KDE1InstallDirs)
include(CheckIncludeFiles)

check_include_files(sys/soundcard.h HAVE_SYS_SOUNDCARD_H)
check_include_files(sun/audioio.h HAVE_SUN_AUDIOIO_H)
set(X_DISPLAY_MISSING FALSE)
configure_file(${PROJECT_SOURCE_DIR}/config.h.in ${PROJECT_BINARY_DIR}/config.h)
include_directories(${PROJECT_BINARY_DIR})

option(ENABLE_SANITIZERS "Enable runtime sanitizing (for development)")
if (ENABLE_SANITIZERS)
    message("Enabling asan and ubsan")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fsanitize=address")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fsanitize=undefined")
endif()

if (CMAKE_COMPILER_IS_GNUCXX)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wno-write-strings")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fpermissive -std=c++98 -Wno-write-strings")
    set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -Wl,--no-undefined")
endif()

add_definitions(-DVERSION=\"0.1.5\")

qt1_wrap_moc(MOC_FILES
    buffer.h
    fft.h
    krecord.h
    level.h
    oss.h
    sound.h
    sunaudio.h
    qtpamainloop.h
)

add_executable(krecord
    krecord.cpp sound.cpp fft.cpp level.cpp buffer.cpp soundfft.c
    oss.cpp sunaudio.cpp
    qtpamainloop.cpp

    ${MOC_FILES}
)
target_link_libraries(krecord
    ${KDE1_KDECORE}
    ${KDE1_KDEUI}
    Qt::Qt1
    X11::X11
    pulse-simple ${PULSEAUDIO_LIBRARY}
)

install(TARGETS krecord RUNTIME DESTINATION ${KDE1_BINDIR})

install_icon(krecord.xpm)
install(FILES krecord.kdelnk DESTINATION ${KDE1_APPSDIR}/Multimedia)

install(FILES index.html DESTINATION ${KDE1_HTMLDIR}/Settings/en/krecord)

install(FILES
    freq.xpm
    level.xpm
    mbackward.xpm
    mforward.xpm
    monitor.xpm
    mpause.xpm
    mplay.xpm
    mrecord.xpm
    mstop.xpm
    DESTINATION ${KDE1_TOOLBARDIR}
)

