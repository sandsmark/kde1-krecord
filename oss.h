/*********************************************************************************
 *
 * $Id: soundcard.h,v 1.3 1999/04/21 22:02:56 kiecza Exp $
 *
 *********************************************************************************/

#ifndef OSS_H
#define OSS_H

#include <qobject.h>
#include <qsocknot.h>

/* ---------------------------------------------------------------------- */
/*  taken from KRecord ...                                                */
/* ---------------------------------------------------------------------- */

#define STATUS_CLOSED    0
#define STATUS_RECORD    1
#define STATUS_PLAYBACK  2

struct pa_stream;

class Soundcard : public QObject
{
    Q_OBJECT;

private:

    /* sound card capabilities */
    char devname[32];
    int  init_done;
    int  afmt_hw;
    int  afmt_sw;
    int  channels_hw;

    int  trigger;
    char driver_name[64];

    /* current settings */
    int  afmt;
    int  channels;
    int  rate;
    int  blocksize;
    int  latency;

    pa_stream *stream;

    /* file handle, reference count */
    int  fd, stat;
    QSocketNotifier *telmi;

    bool stopped;

    /* internal functions */
    void get_capabilities();
    int  open_dev(int record);
    void close_dev();

    static void pulse_write_cb(pa_stream *stream, size_t length, void *userdata);
    static void pulse_read_cb(pa_stream *stream, size_t length, void *userdata);

public:
    char buffer[65536];

    Soundcard(char *dev);
    ~Soundcard();
    char *driver();
    void setparams(struct SOUNDPARAMS *params);
    int  get_blocksize()
    {
        return blocksize;
    };
    int  start_record();
    int  start_playback();
    int  kill_buffer();
    int  stop();

    int  has_channels();      /* # of channels (1=mono,2=stereo) */
    int  has_format(int f);   /* check format availibity         */

public slots:
    void sounddata(int);
private slots:
    void emitData();

signals:

    void senddata(void*);
//    void senddata(void *data);
    /* !!! only one should be connected to receivedata !!! */
    void receivedata(void *data);
    void newparams(struct SOUNDPARAMS *params);
};

#endif

