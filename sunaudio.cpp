#include "config.h"
#ifdef HAVE_SUN_AUDIOIO_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>

#include <sys/ioctl.h>
#include <sun/audioio.h>

#include "sound.h"
#include "sunaudio.moc"

/* ---------------------------------------------------------------------- */

Soundcard::Soundcard(char *dev)
{
    if (dev)
	strcpy(devname,dev);
    else
	strcpy(devname,"/dev/audio");
    
    strcpy(driver_name,"sunaudio";
    get_capabilities();
    /* TODO: rate/format/etc. init */
    fd = -1;
}

Soundcard::~Soundcard()
{
    /* nothing */
}

int
Soundcard::start_record()
{
    switch (stat) {
    case STATUS_CLOSED:
	if (!init_done)
	    get_capabilities();
	if (!init_done)
	    return -1;
	return open_dev(TRUE);
    case STATUS_RECORD:
	return 0;
    case STATUS_PLAYBACK:
	close_dev();
	return open_dev(TRUE);
    }
    return -1;
}

int
Soundcard::start_playback()
{
    switch (stat) {
    case STATUS_CLOSED:
	if (!init_done)
	    get_capabilities();
	if (!init_done)
	    return -1;
	return open_dev(FALSE);
    case STATUS_RECORD:
	close_dev();
	return open_dev(FALSE);
    case STATUS_PLAYBACK:
	return 0;
    }
    return -1;
}

int
Soundcard::kill_buffer()
{
    /* XXX */
    return 0;
}

int
Soundcard::stop()
{
    if (stat != STATUS_CLOSED)
	close_dev();
    return 0;
}

/* ---------------------------------------------------------------------- */

void
Soundcard::get_capabilities()
{
    int dsp;
    
    if (-1 != (dsp = open(devname, O_RDONLY))) {
	
	/* XXX */
	
        close(dsp);
	init_done = 1;

    } else {
	init_done = 0;
    }
}

int
Soundcard::has_channels()
{
    if (!init_done)
	return -1;
    /* XXX */
}

int
Soundcard::has_format(int f)
{
    if (!init_done)
	return -1;
    switch (f) {
    case FMT_8BIT:
    case FMT_16BIT:
    case FMT_MULAW:
    case FMT_ALAW:
    default:
	return 0;
    }
}

char*
Soundcard::driver()
{
    return driver_name;
}

int
Soundcard::open_dev(int record)
{
    struct SOUNDPARAMS p;

    if (-1 == (fd = open(devname,record ? O_RDONLY : O_WRONLY)))
        goto err;
    fcntl(fd,F_SETFD,FD_CLOEXEC);

    /* XXX */

    p.channels  = channels;
    p.rate      = rate;
    p.blocksize = blocksize;
    p.latency   = latency;
    /* XXX format */
    emit newparams(&p);

    return 0;

err:
    if (-1 != fd)
        close(fd);
    stat = STATUS_CLOSED;
    fd = -1;
    return -1;
}

void
Soundcard::close_dev()
{
    close(fd);
    fd = -1;
    stat = STATUS_CLOSED;

    delete telmi;
    return;
}

void
Soundcard::setparams(struct SOUNDPARAMS *p)
{
    /* XXX */

    switch (stat) {
    case STATUS_RECORD:    
	close_dev();
	open_dev(TRUE);
	break;
    case STATUS_PLAYBACK:
	close_dev();
	open_dev(FALSE);
	break;
    case STATUS_CLOSED:
	if (!init_done)
	    get_capabilities();
	if (!init_done)
	    return;
	if (0 == open_dev(TRUE))
	    close_dev();
	break;
    }
}

void
Soundcard::sounddata(int s)
{
    switch (stat) {
    case STATUS_RECORD:
	read(fd,buffer,blocksize);
	emit senddata((void*)buffer);
	break;
    case STATUS_PLAYBACK:
	emit receivedata((void*)buffer);
	write(fd,buffer,blocksize);
	emit senddata((void*)buffer); /* fft :-) */
	break;
    }
}

#endif
