#include <stdio.h>
#include <iostream>
#include <stdlib.h> 
#include <unistd.h> 
#include <fcntl.h>
#include <sys/ioctl.h>

#include <qkeycode.h>
#include <qlabel.h>
#include <qaccel.h>
#include <qfiledlg.h>

#include <kiconloader.h>
#include <klocale.h>
#include <kurl.h>

#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/Xmu/WinUtil.h>	/* for XmuClientWindow() */

#include "sound.h"
#include "fft.h"
#include "level.h"
#include "buffer.h"
#include "krecord.moc"
#include "qtpamainloop.h"

#define _(TEXT) klocale->translate(TEXT)

#define STAT_LATENCY       1
#define STAT_RATE          2
#define STAT_CHANNELS      3
#define STAT_FORMAT        4
#define STAT_MISC          5


KApplication *globalKapp;
KIconLoader  *globalKIL;
KLocale      *globalKlocale;

pa_mainloop_api *pulse_api = NULL;

/* ------------------------------------------------------------------------ */

int main(int argc, char **argv)
{
    int      i;
    globalKapp     = new KApplication( argc, argv, "krecord");
    globalKIL      = globalKapp->getIconLoader();
    globalKlocale  = globalKapp->getLocale();
    QtPaMainLoop mainloop;
    pulse_api = &mainloop.pa_vtable;
    if (!connectToPulse()) {
        std::cerr << "Failed to connect to pulse, aborting" << std::endl;
        exit(1);
    }

    KRecord *krecord;

    krecord = new KRecord();

    for (i = 1; i < argc; i++)
	krecord->blist->add_filebuffer(argv[i]);

    return globalKapp->exec();
}

/* ------------------------------------------------------------------------ */

KRecord::KRecord() : KTopLevelWidget("main")
{
    int      i = -1;

    soundcard  = new Soundcard(NULL);
    soundopts  = new SoundOptions(soundcard,"soundopts");
    kfft       = new KFFT(soundcard);
    klevel     = new KLevel(soundcard);
    listwidget = new QListBox(this,"bufferlist");
    blist      = new BufferList(listwidget,soundcard);
    fdialog    = new QFileDialog(NULL,"*.wav",NULL,"fdialog",TRUE);
    dropzone   = new KDNDDropZone(this, DndURL);
    accel      = new QAccel(this);

    globalKapp->setMainWidget(this);
    setView(listwidget);
    create_menu();
    create_toolbar();
    create_soundbar();
    create_statusline();

    accel->connectItem(accel->insertItem(Key_Enter),  blist,SLOT(play()));
    accel->connectItem(accel->insertItem(Key_Return), blist,SLOT(play()));
    accel->connectItem(accel->insertItem(Key_Escape), blist,SLOT(stop()));
    accel->connectItem(accel->insertItem(Key_R),      blist,SLOT(record()));
    accel->connectItem(accel->insertItem(Key_N),
		       blist,SLOT(switch_to_new_buffer()));
#if 1
    accel->connectItem(accel->insertItem(Key_Space),
		       blist,SLOT(switch_to_new_buffer()));
#endif

    connect(soundcard,SIGNAL(newparams(struct SOUNDPARAMS*)),
	    this, SLOT(update_statusline(struct SOUNDPARAMS*)));
    connect(blist,SIGNAL(status(char*)),
	    this, SLOT(update_statusline(char*)));
    connect(soundopts,SIGNAL(set_level(int)),
	    blist, SLOT(set_level(int)));
    connect(dropzone,SIGNAL(dropAction(KDNDDropZone*)), 
	    this,SLOT(drop(KDNDDropZone*)));

    /* session management */
    if (globalKapp->isRestored()) {
	for (i = 1; canBeRestored(i); i++)
	    if (0 == strcmp(classNameOfToplevel(i),"KRecord"))
		break;
	if (!canBeRestored(i))
	    i = -1;
    }
    if (i > 0) {
	restore(i);
    } else {
	resize(400,250);
	show();
    }
    
    blist->monitor();
}

KRecord::~KRecord()
{
    delete main_menu;
    delete help_menu;
    delete opt_menu;
    delete file_menu;

    delete toolbar;
    delete soundbar;
    delete statusline;

    delete blist;
    delete listwidget;
    delete dropzone;

    delete fdialog;
    delete kfft;
    delete soundopts;
    delete soundcard;
}

void
KRecord::create_menu()
{
    file_menu = new QPopupMenu;
    file_menu->insertItem( _("&New memory buffer"), blist, SLOT(new_ram()));
    file_menu->insertItem( _("New &file buffer..."), this, SLOT(new_file()));
    file_menu->insertItem( _("&Save buffer as..."), this, SLOT(save_as()));
    file_menu->insertSeparator();
    file_menu->insertItem( _("&Delete buffer"), blist, SLOT(del_buf()));
    file_menu->insertSeparator();
    file_menu->insertItem( _("&Quit"), this, SLOT(quit_cb()), CTRL+Key_Q);

    opt_menu = new QPopupMenu;
    opt_menu->insertItem
	( _("&Sound Options..."), this, SLOT(record_options()));
    opt_menu->insertItem
	( _("&Freq Spectrum..."), kfft, SLOT(showit()));
    opt_menu->insertItem
	( _("&Input Level..."), klevel, SLOT(showit()));
    opt_menu->insertItem
	( _("Run &Mixer"), this, SLOT(exec_mixer()));
    opt_menu->insertSeparator();
    tb_mid = opt_menu->insertItem
	( _("Hide &Toolbar"),this,SLOT(tb_toggle()));
    sl_mid = opt_menu->insertItem
	( _("Hide Status&line"),this,SLOT(sl_toggle()));

    help_menu = new QPopupMenu;
    help_menu->insertItem( _("&Help"), this, SLOT(help_cb()),Key_F1);
    help_menu->insertSeparator();
    help_menu->insertItem( _("&About..."), this, SLOT(about_cb()));

    main_menu = new KMenuBar(this, "main menu");
    main_menu->insertItem( _("&File"), file_menu);
    main_menu->insertItem( _("&Options"), opt_menu);
    main_menu->insertSeparator();
    main_menu->insertItem( _("&Help"), help_menu);

    setMenu(main_menu);
}

void
KRecord::create_toolbar()
{
    toolbar = new KToolBar( this );
    KIconLoader *loader = kapp->getIconLoader();
    QPixmap pixmap;
    loader->insertDirectory(0,"."); /* handy for testing without install */

    pixmap = loader->loadIcon("filenew.xpm");
    toolbar->insertButton
	(pixmap, 0, SIGNAL(clicked()), blist, SLOT(new_ram()), TRUE,
	 _("New memory buffer"));

    pixmap = loader->loadIcon("filefloppy.xpm");
    toolbar->insertButton
	(pixmap, 0, SIGNAL(clicked()), this, SLOT(save_as()), TRUE,
	 _("Save buffer"));

    toolbar->insertSeparator();
    pixmap = loader->loadIcon("freq.xpm");
    toolbar->insertButton
	(pixmap, 0, SIGNAL(clicked()), kfft, SLOT(showit()), TRUE,
	 _("Freq Spectrum"));

    pixmap = loader->loadIcon("level.xpm");
    toolbar->insertButton
	(pixmap, 0, SIGNAL(clicked()), klevel, SLOT(showit()), TRUE,
	 _("Input Level"));

    toolbar->insertSeparator();
    pixmap = loader->loadIcon("help.xpm");
    toolbar->insertButton
	(pixmap, 0, SIGNAL(clicked()), this, SLOT(help_cb()), TRUE,
	 _("Help"));

    toolbar->insertSeparator();
    pixmap = loader->loadIcon("exit.xpm");
    toolbar->insertButton
	(pixmap, 0, SIGNAL(clicked()), this, SLOT(quit_cb()), TRUE, _("Quit"));

    toolbar->setBarPos(KToolBar::Top);
    tb_id = addToolBar(toolbar);
    tb_map = 1;
}

void
KRecord::create_soundbar()
{
    soundbar = new KToolBar( this );
    KIconLoader *loader = kapp->getIconLoader();
    QPixmap pixmap;

    pixmap = loader->loadIcon("mrecord.xpm");
    soundbar->insertButton
	(pixmap, 0, SIGNAL(clicked()), blist, SLOT(record()), TRUE,
	 _("Start Record"));

    pixmap = loader->loadIcon("mstop.xpm");
    soundbar->insertButton
	(pixmap, 0, SIGNAL(clicked()), blist, SLOT(stop()), TRUE,
	 _("Stop Record/Playback"));

    pixmap = loader->loadIcon("mplay.xpm");
    soundbar->insertButton
	(pixmap, 0, SIGNAL(clicked()), blist, SLOT(play()), TRUE,
	 _("Start Playback"));

    pixmap = loader->loadIcon("mbackward.xpm");
    soundbar->insertButton
	(pixmap, 0, SIGNAL(clicked()), this, SLOT(nop_cb()), TRUE,
	 _("Back (not implemented yet)"));

    pixmap = loader->loadIcon("mforward.xpm");
    soundbar->insertButton
	(pixmap, 0, SIGNAL(clicked()), this, SLOT(nop_cb()), TRUE,
	 _("Forward (not implemented yet)"));

    soundbar->insertSeparator();
    pixmap = loader->loadIcon("monitor.xpm");
    soundbar->insertButton
	(pixmap, 0, SIGNAL(clicked()), blist, SLOT(monitor()), TRUE,
	 _("Turn on/off monitor"));

    soundbar->setBarPos(KToolBar::Top);
    sb_id = addToolBar(soundbar);
}

void
KRecord::create_statusline()
{
    statusline = new KStatusBar(this);
    statusline->insertItem("999 ms",      STAT_LATENCY);
    statusline->insertItem("xxxxxxxxxx",  STAT_FORMAT);
    statusline->insertItem("xxxxxxx",     STAT_CHANNELS);
    statusline->insertItem("9999999",     STAT_RATE);
    statusline->insertItem("-",           STAT_MISC);
    statusline->setInsertOrder(KStatusBar::RightToLeft);
    setStatusBar(statusline);
    sl_map = 1;
}

void
KRecord::update_statusline(struct SOUNDPARAMS *p)
{
    char text[32];
    
    statusline->changeItem((1==p->channels) ? "mono":"stereo", STAT_CHANNELS);
    sprintf(text,"%d",p->rate);
    statusline->changeItem(text,STAT_RATE);
    sprintf(text,"%d ms",p->latency);
    statusline->changeItem(text,STAT_LATENCY);
    statusline->changeItem(sndfmt2str(p->format),STAT_FORMAT);
}

void
KRecord::update_statusline(char *text)
{
    statusline->changeItem(text,STAT_MISC);
}


/* ------------------------------------------------------------------------ */

void KRecord::nop_cb()
{
    QMessageBox::about(this, "sorry...",
		       "not implemented yet");
}

void KRecord::new_file()
{
    QString filename;

    if (NULL == (filename = fdialog->getSaveFileName
		 (NULL,"*.wav",NULL,"fdialog")))
	return;
    blist->add_filebuffer(filename);
}

void KRecord::save_as()
{
    QString filename;

    if (NULL == (filename = fdialog->getSaveFileName
		 (NULL,"*.wav",NULL,"fdialog")))
	return;
    blist->save_buf(filename);
}

void KRecord::quit_cb()
{
    delete this;
    globalKapp->quit();
}

void KRecord::record_options()
{
    soundopts->show();
}

void KRecord::exec_mixer()
{
    if (!fork()) {
	execlp("kmix","kmix",NULL);
	exit(0);
    }
    /* XXX zombie */
}

void KRecord::tb_toggle()
{
    tb_map = !tb_map;
    enableToolBar(tb_map ? KToolBar::Show : KToolBar::Hide, tb_id);
    opt_menu->changeItem
	(tb_map ? _("Hide &Toolbar") : _("Show &Toolbar"), tb_mid);
}

void KRecord::sl_toggle()
{
    sl_map = !sl_map;
    enableStatusBar(sl_map ? KStatusBar::Show : KStatusBar::Hide);
    opt_menu->changeItem
	(sl_map ? _("Hide Status&line") : _("Show Status&line"), sl_mid);
}

void KRecord::help_cb()
{
    globalKapp->invokeHTMLHelp("","");    
}

void KRecord::about_cb()
{
    QMessageBox::about(this, "About...",
		       "krecord 1.1\n\n"
		       "(c) 1997,98 Gerd Knorr <kraxel@goldbach.in-berlin.de>");
}

/* ------------------------------------------------------------------------ */

void KRecord::drop(KDNDDropZone* zone)
{
    QStrList strlist = zone->getURLList();
    QString  *url    = new QString(strlist.first());
    const char *h;

    while ((const char*)*url) {
	h = (const char*)*url;
	if (0 == strncmp(h,"file:",5))
	    blist->add_filebuffer(h+5);
	delete url;
	url = new QString(strlist.next());
    }    
}

/* ------------------------------------------------------------------------ */

KFFT::KFFT(Soundcard *card) : KTopLevelWidget("fft")
{
    int     i = -1;
    fftwin    = new FFTWindow(this,"fft");
    setView(fftwin);
    QObject::connect(card,SIGNAL(senddata(void*)),
		     fftwin, SLOT(new_data(void*)));
    QObject::connect(card,SIGNAL(newparams(struct SOUNDPARAMS*)),
		     fftwin, SLOT(new_params(struct SOUNDPARAMS*)));

    setCaption("freq spectrum");

#if 1
    /* session management */
    if (globalKapp->isRestored()) {
	for (i = 1; canBeRestored(i); i++)
	    if (0 == strcmp(classNameOfToplevel(i),"KFFT"))
		break;
	if (!canBeRestored(i))
	    i = -1;
    }
    if (i > 0) {
	restore(i);
    } else {
	resize(200,120);
    }
#else
    resize(200,120);
#endif
}

KFFT::~KFFT()
{
    delete fftwin;
}

void
KFFT::showit()
{
    if (!isVisible())
	show();
}

/* ------------------------------------------------------------------------ */

KLevel::KLevel(Soundcard *card) : KTopLevelWidget("level")
{
    levelwin = new LevelWindow(this,"level");
    setView(levelwin);
    QObject::connect(card,SIGNAL(senddata(void*)),
                     levelwin, SLOT(new_data(void*)));
    QObject::connect(card,SIGNAL(newparams(struct SOUNDPARAMS*)),
                     levelwin, SLOT(new_params(struct SOUNDPARAMS*)));

    setCaption("input level");
    /* setMaximumSize(320,160); */
    resize(200,30);
}

KLevel::~KLevel()
{
    delete levelwin;
}

void
KLevel::showit()
{
   if (!isVisible())
      show();
}
