#ifndef SUNAUDIO_H
#define SUNAUDIO_H

#include <qobject.h>
#include <qsocknot.h>

/* ---------------------------------------------------------------------- */

#define STATUS_CLOSED    0
#define STATUS_RECORD    1
#define STATUS_PLAYBACK  2

class Soundcard : public QObject
{
    Q_OBJECT;

private:
    /* sound card capabilities */
    char devname[32];
    char driver_name[64];
    int  init_done;

    /* current settings */
    /* XXX */
    int  channels;
    int  rate;
    int  blocksize;
    int  latency;

    /* file handle, reference count */
    int  fd, stat;
    char buffer[65536];
    QSocketNotifier *telmi;
    
    /* internal functions */
    void get_capabilities();
    int  open_dev(int record);
    void close_dev();

public:
    Soundcard(char *dev);
    ~Soundcard();
    char *driver();
    void setparams(struct SOUNDPARAMS *params);
    int  start_record();
    int  start_playback();
    int  kill_buffer();
    int  stop();

    int  has_channels();      /* # of channels (1=mono,2=stereo) */
    int  has_format(int f);   /* check format availibity         */ 

public slots:
    void sounddata(int);
    
signals:
    void senddata(void *data);
    /* !!! only one should be connected to receivedata !!! */
    void receivedata(void *data);
    void newparams(struct SOUNDPARAMS *params);
};

#endif
